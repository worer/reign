const dotenv = require('dotenv');
const articles = require('../data/articles');
const Article = require('../api/article/article.model');
const connectDB = require('./database');

dotenv.config();
connectDB();

const importArticles = async () => {
  try {
    await Article.deleteMany();

    await Article.insertMany(articles);

    console.log('Data Imported');
    process.exit();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

const deleteArticles = async () => {
  try {
    await Article.deleteMany();

    console.log('Data destroyed');
    process.exit();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

switch (process.argv[2]) {
  case '-d': {
    deleteArticles();
    break;
  }
  default: {
    importArticles();
  }
}
