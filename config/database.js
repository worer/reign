const mongoose = require('mongoose');

async function connectDB() {
  try {
    const URI = process.env.NODE_ENV === 'development' ? process.env.DB_URI : process.env.DB_URI_TEST;
    await mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('MongoDB Connected');
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
}

module.exports = connectDB;
