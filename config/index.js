require('dotenv').config();

const all = {
  env: process.env.NODE_ENV,

  // Server port
  port: process.env.PORT || 8080,

  // Should we populate the DB with sample data?
  seedDB: process.env.NODE_ENV !== 'production',

  // Secret for session, you will want to change this and make it an environment variable

};

module.exports = all;
