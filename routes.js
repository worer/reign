const articles = require('./api/article');

function routes(app) {
  app.use('/api/articles', articles);
}

module.exports = routes;
