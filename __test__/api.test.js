/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
const mongoose = require('mongoose');
const supertest = require('supertest');
const { app, server } = require('../app');
const Article = require('../api/article/article.model');

const api = supertest(app);

const initialArticles = [
  {
    _id: '622040c452849a4edb179aa4',
    title: null,
    url: null,
    author: 'ahungry',
    points: null,
    story_text: null,
    comment_text: 'It&#x27;d be nice if someone on the Snowflake team actually maintained their JS adapter... (<a href="https:&#x2F;&#x2F;github.com&#x2F;snowflakedb&#x2F;snowflake-connector-nodejs&#x2F;pulls" rel="nofollow">https:&#x2F;&#x2F;github.com&#x2F;snowflakedb&#x2F;snowflake-connector-nodejs&#x2F;pu...</a>)',
    num_comments: null,
    story_id: 30533799,
    story_title: 'Snowflake acquires Streamlit for $800M to help customers build data-based apps',
    story_url: 'https://techcrunch.com/2022/03/02/snowflake-acquires-streamlit-for-800m-to-help-customers-build-data-based-apps/',
    parent_id: 30533799,
    created_at_i: 1646277903,
    _tags: ['comment', 'author_ahungry', 'story_30533799'],
    objectID: 30536770,
    _highlightResult: {
      author: { value: 'ahungry', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "It'd be nice if someone on the Snowflake team actually maintained their JS adapter... (<a href=\"https://github.com/snowflakedb/snowflake-connector-nodejs/pulls\" rel=\"nofollow\">https://github.com/snowflakedb/snowflake-connector-<em>nodejs</em>/pu...</a>)", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'Snowflake acquires Streamlit for $800M to help customers build data-based apps', matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://techcrunch.com/2022/03/02/snowflake-acquires-streamlit-for-800m-to-help-customers-build-data-based-apps/', matchLevel: 'none', matchedWords: [] },
    },
  },
  {
    _id: '622040c452849a4edb179aa6',
    title: null,
    url: null,
    author: 'toast0',
    points: null,
    story_text: null,
    comment_text: 'In a &#x27;well designed system&#x27;, any given node has limited access to the other nodes and user data.<p>If a node is compromised, that shouldn&#x27;t turn into full access to everything. Specifics would vary depending on your application, but while most user facing servers might need to be able to validate a login token, few would need to be able to issue a login token or access billing details.<p>If you compromise the node that has billing details or access to them, then you&#x27;ll have them, but if you compromise a different node, you won&#x27;t. That&#x27;s better than compromise any host and get access to the &#x27;private&#x27; network where there&#x27;s no auth or encryption, which is kind of traditional.',
    num_comments: null,
    story_id: 30530592,
    story_title: 'The new White House memo on zero trust is a strong signal',
    story_url: 'https://www.pomerium.com/blog/white-house-zt-memo/',
    parent_id: 30534205,
    created_at_i: 1646267031,
    _tags: ['comment', 'author_toast0', 'story_30530592'],
    objectID: 30535439,
    _highlightResult: {
      author: { value: 'toast0', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "In a 'well designed system', any given node has limited access to the other <em>nodes</em> and user data.<p>If a node is compromised, that shouldn't turn into full access to everything. Specifics would vary depending on your application, but while most user facing servers might need to be able to validate a login token, few would need to be able to issue a login token or access billing details.<p>If you compromise the node that has billing details or access to them, then you'll have them, but if you compromise a different node, you won't. That's better than compromise any host and get access to the 'private' network where there's no auth or encryption, which is kind of traditional.", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'The new White House memo on zero trust is a strong signal', matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://www.pomerium.com/blog/white-house-zt-memo/', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'bscphil',
    points: null,
    story_text: null,
    comment_text: 'Well said. Dynamically linked packages managed by a package manager and created and distributed by maintainers is the Linux Way of doing things for multiple very good reasons.<p>In fact, the article brilliant describes one of them:<p>&gt; Modern software distribution: AKA &quot;actually free with no-ads apps store&quot; of the Linux and BSD worlds.<p>That&#x27;s it. That&#x27;s the advantage summed up in a single description that wasn&#x27;t even <i>intended</i> as a description of the advantages of the current way of doing things!<p>The article has a list of &quot;cons&quot;, but it utterly fails to consider the possibility that this <i>overwhelming</i> advantage of what it calls &quot;modern software distribution&quot; is simply and inescapably tied to the <i>maintainer-oriented approach</i> that currently underlies it!<p>Attempts to create modes of distribution that are even <i>slightly</i> different from maintainer-distribution (think Google Play) <i>suck ass</i>. For all its faults, F-droid is a vastly better platform because it insists that the software must be <i>built</i> and <i>distributed</i> by an F-droid maintainer. Stepping even further away into the realm of developer built opaque binaries is begging for chaos and misery.<p>As you say, it&#x27;s ultimately a security concern [1]. The article claims that a change is necessary because of &quot;increased usage of languages and run-times that don&#x27;t fit the current build model&quot;, &quot;some of these new projects have large numbers ... of dependencies&quot;, but these are themselves <i>problems</i> with modern software development. Even putting aside distribution, bloated nodejs dependency trees create security vulnerabilities. The inability to develop software without pinning <i>exact</i> versions of your dependencies (which then need to be manually upgraded by the developer) creates security vulnerabilities and fragility. These are <i>problems</i>, not good reasons for changing our current way of distributing software!<p>I&#x27;m convinced that there are some (like me) willing to die on this particular hill. Come what may, even if half of the developers out there switch to Go and only ship static binaries, we&#x27;re going to continue working on and using traditional Linux distributions with maintainer controlled software. (To be clear: Go and static builds <i>are</i> warranted in many cases. For example, closed-source rarely updated software like games, and software that is &quot;deployed&quot; rather than installed. But these are not the base case for Linux distributions.)<p>Further reading: <a href="http:&#x2F;&#x2F;kmkeen.com&#x2F;maintainers-matter&#x2F;" rel="nofollow">http:&#x2F;&#x2F;kmkeen.com&#x2F;maintainers-matter&#x2F;</a><p>[1] <a href="https:&#x2F;&#x2F;blogs.gentoo.org&#x2F;mgorny&#x2F;2021&#x2F;02&#x2F;19&#x2F;the-modern-packagers-security-nightmare&#x2F;" rel="nofollow">https:&#x2F;&#x2F;blogs.gentoo.org&#x2F;mgorny&#x2F;2021&#x2F;02&#x2F;19&#x2F;the-modern-packag...</a>',
    num_comments: null,
    story_id: 30528602,
    story_title: 'Single binary executable packages',
    story_url: 'https://notes.volution.ro/v1/2022/01/notes/fbf3f06c/',
    parent_id: 30529254,
    created_at_i: 1646269835,
    _tags: ['comment', 'author_bscphil', 'story_30528602'],
    objectID: 30535820,
    _highlightResult: {
      author: { value: 'bscphil', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "Well said. Dynamically linked packages managed by a package manager and created and distributed by maintainers is the Linux Way of doing things for multiple very good reasons.<p>In fact, the article brilliant describes one of them:<p>&gt; Modern software distribution: AKA &quot;actually free with no-ads apps store&quot; of the Linux and BSD worlds.<p>That's it. That's the advantage summed up in a single description that wasn't even <i>intended</i> as a description of the advantages of the current way of doing things!<p>The article has a list of &quot;cons&quot;, but it utterly fails to consider the possibility that this <i>overwhelming</i> advantage of what it calls &quot;modern software distribution&quot; is simply and inescapably tied to the <i>maintainer-oriented approach</i> that currently underlies it!<p>Attempts to create modes of distribution that are even <i>slightly</i> different from maintainer-distribution (think Google Play) <i>suck ass</i>. For all its faults, F-droid is a vastly better platform because it insists that the software must be <i>built</i> and <i>distributed</i> by an F-droid maintainer. Stepping even further away into the realm of developer built opaque binaries is begging for chaos and misery.<p>As you say, it's ultimately a security concern [1]. The article claims that a change is necessary because of &quot;increased usage of languages and run-times that don't fit the current build model&quot;, &quot;some of these new projects have large numbers ... of dependencies&quot;, but these are themselves <i>problems</i> with modern software development. Even putting aside distribution, bloated <em>nodejs</em> dependency trees create security vulnerabilities. The inability to develop software without pinning <i>exact</i> versions of your dependencies (which then need to be manually upgraded by the developer) creates security vulnerabilities and fragility. These are <i>problems</i>, not good reasons for changing our current way of distributing software!<p>I'm convinced that there are some (like me) willing to die on this particular hill. Come what may, even if half of the developers out there switch to Go and only ship static binaries, we're going to continue working on and using traditional Linux distributions with maintainer controlled software. (To be clear: Go and static builds <i>are</i> warranted in many cases. For example, closed-source rarely updated software like games, and software that is &quot;deployed&quot; rather than installed. But these are not the base case for Linux distributions.)<p>Further reading: <a href=\"http://kmkeen.com/maintainers-matter/\" rel=\"nofollow\">http://kmkeen.com/maintainers-matter/</a><p>[1] <a href=\"https://blogs.gentoo.org/mgorny/2021/02/19/the-modern-packagers-security-nightmare/\" rel=\"nofollow\">https://blogs.gentoo.org/mgorny/2021/02/19/the-modern-packag...</a>", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'Single binary executable packages', matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://notes.volution.ro/v1/2022/01/notes/fbf3f06c/', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'ashwagary',
    points: null,
    story_text: null,
    comment_text: 'This view is a limitation of your understanding of Bitcoin.<p>Layer 2 solutions like lightning and liquid network are built to scale to infinite transactions per seconds at near $0 cost if p2p participants choose that fee price point, which many current routing nodes do.',
    num_comments: null,
    story_id: 30528453,
    story_title: "Crypto exchanges won't bar Russians, raising fears of sanctions backdoor",
    story_url: 'https://www.reuters.com/markets/europe/crypto-exchanges-wont-bar-russians-raising-fears-sanctions-backdoor-2022-03-02/',
    parent_id: 30533477,
    created_at_i: 1646257132,
    _tags: ['comment', 'author_ashwagary', 'story_30528453'],
    objectID: 30533545,
    _highlightResult: {
      author: { value: 'ashwagary', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: 'This view is a limitation of your understanding of Bitcoin.<p>Layer 2 solutions like lightning and liquid network are built to scale to infinite transactions per seconds at near $0 cost if p2p participants choose that fee price point, which many current routing <em>nodes</em> do.', matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: "Crypto exchanges won't bar Russians, raising fears of sanctions backdoor", matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://www.reuters.com/markets/europe/crypto-exchanges-wont-bar-russians-raising-fears-sanctions-backdoor-2022-03-02/', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'mnurzia',
    points: null,
    story_text: null,
    comment_text: 'The arena design pattern is something I&#x27;m using a lot for a project in C. Like Rust, the primary reason it&#x27;s attractive to use is because of how it plays well with memory safety. Although I&#x27;m no Rust expert, I suspect that its ownership model makes designing tree data structures difficult and unwieldy. In C, it is similarly unwieldy to design traditional tree structures because of how hard it is to manage pointers everywhere. If each Tree is a pointer to a dynamically-allocated block of data, it&#x27;s really hard to remember when to call free(), especially if you have a function that modifies a Node in place by rearranging (or possibly deleting) its children, which is often the case for ASTs. In contrast, the arena pattern uses one flat memory allocation, and allows a couple optimizations:<p>- References between nodes can be whatever type you want. In most cases, I don&#x27;t need a full size_t or the width of a pointer to represent a reference to another node, and a uint32_t or the likes will do. (This is faster on some platforms.)<p>- The whole arena is a single memory allocation (usually a vector.) Rather than allocating on every node, you can allocate memory for a few nodes and then use it until it runs out, at which point you can allocate memory for a few more.<p>- Since it&#x27;s a single allocation, destroying the entire tree is trivial: just free the block of memory. Importantly, this doesn&#x27;t require the recursive algorithm that a pointer-based tree might use to free itself, so you run no risk of blowing out your stack if some user creates a really deep tree.<p>- You can do repeated node removal&#x2F;insertion in a really efficient way using a neat trick. It&#x27;s possible to maintain an &quot;empty list&quot; by using the memory reclaimed from removed nodes in the arena to represent a linked list of empty positions. Then, if you want to add a new node later, you can use the last element of the empty list as the position for your next node, without resizing the arena. For an AST, this can be crucial because some algorithms will thrash the tree by doing lots of insertions and removals. (This technique is not type-safe in C, however, and can be UB if you do it wrong, so you have to be really careful, and it&#x27;s a bit of a code smell.)<p>- Since the tree is stored in contiguous memory, it&#x27;s a little more cache-friendly than a bunch of pointers that are potentially scattered throughout the heap. This is a nice bonus.<p>- The structure generalizes to any type of graph, like a linked list. If you need a structure that can represent any kind of link between objects without pointers, the arena will do.<p>There are a few cons, however:<p>- Like a pointer-based structure, you still have to remember to update edges in the tree when you update its nodes. Instead of updating a Node*, however, you&#x27;re updating a NodeID, and sometimes less obvious when to do this.<p>- Since the array is a large contiguous allocation, its effect on your heap is very different than the pointer strategy that uses tons of small allocations. This gives different heap fragmentation characteristics, which might be better or worse depending on what allocator you use.<p>- Because a NodeID is just a position in an array, you likely won&#x27;t get segfaults when you access a node that was freed, or a NULL Node*. This is actually a bad thing because you don&#x27;t catch certain bugs until way later, when the corrupted tree is used. In C, I&#x27;ve found that a healthy amount of assert()s helps a lot but it requires a lot of abstraction and makes the code significantly more verbose.<p>All in all it&#x27;s just another example of balancing performance, complexity, and ease of development. Like any programming tool, it&#x27;s worth considering, but not really necessary unless you really need the speed (C) or unless it actually makes your code easier to use (Rust).*',
    num_comments: null,
    story_id: 30529077,
    story_title: 'TreeFlat: Building a (possible) faster tree for Rust, inspired by APL',
    story_url: 'https://www.elmalabarista.com/blog/2022-flat-tree/',
    parent_id: 30529077,
    created_at_i: 1646256469,
    _tags: ['comment', 'author_mnurzia', 'story_30529077'],
    objectID: 30533428,
    _highlightResult: {
      author: { value: 'mnurzia', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "The arena design pattern is something I'm using a lot for a project in C. Like Rust, the primary reason it's attractive to use is because of how it plays well with memory safety. Although I'm no Rust expert, I suspect that its ownership model makes designing tree data structures difficult and unwieldy. In C, it is similarly unwieldy to design traditional tree structures because of how hard it is to manage pointers everywhere. If each Tree is a pointer to a dynamically-allocated block of data, it's really hard to remember when to call free(), especially if you have a function that modifies a Node in place by rearranging (or possibly deleting) its children, which is often the case for ASTs. In contrast, the arena pattern uses one flat memory allocation, and allows a couple optimizations:<p>- References between <em>nodes</em> can be whatever type you want. In most cases, I don't need a full size_t or the width of a pointer to represent a reference to another node, and a uint32_t or the likes will do. (This is faster on some platforms.)<p>- The whole arena is a single memory allocation (usually a vector.) Rather than allocating on every node, you can allocate memory for a few <em>nodes</em> and then use it until it runs out, at which point you can allocate memory for a few more.<p>- Since it's a single allocation, destroying the entire tree is trivial: just free the block of memory. Importantly, this doesn't require the recursive algorithm that a pointer-based tree might use to free itself, so you run no risk of blowing out your stack if some user creates a really deep tree.<p>- You can do repeated node removal/insertion in a really efficient way using a neat trick. It's possible to maintain an &quot;empty list&quot; by using the memory reclaimed from removed <em>nodes</em> in the arena to represent a linked list of empty positions. Then, if you want to add a new node later, you can use the last element of the empty list as the position for your next node, without resizing the arena. For an AST, this can be crucial because some algorithms will thrash the tree by doing lots of insertions and removals. (This technique is not type-safe in C, however, and can be UB if you do it wrong, so you have to be really careful, and it's a bit of a code smell.)<p>- Since the tree is stored in contiguous memory, it's a little more cache-friendly than a bunch of pointers that are potentially scattered throughout the heap. This is a nice bonus.<p>- The structure generalizes to any type of graph, like a linked list. If you need a structure that can represent any kind of link between objects without pointers, the arena will do.<p>There are a few cons, however:<p>- Like a pointer-based structure, you still have to remember to update edges in the tree when you update its <em>nodes</em>. Instead of updating a Node*, however, you're updating a NodeID, and sometimes less obvious when to do this.<p>- Since the array is a large contiguous allocation, its effect on your heap is very different than the pointer strategy that uses tons of small allocations. This gives different heap fragmentation characteristics, which might be better or worse depending on what allocator you use.<p>- Because a NodeID is just a position in an array, you likely won't get segfaults when you access a node that was freed, or a NULL Node*. This is actually a bad thing because you don't catch certain bugs until way later, when the corrupted tree is used. In C, I've found that a healthy amount of assert()s helps a lot but it requires a lot of abstraction and makes the code significantly more verbose.<p>All in all it's just another example of balancing performance, complexity, and ease of development. Like any programming tool, it's worth considering, but not really necessary unless you really need the speed (C) or unless it actually makes your code easier to use (Rust).*", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'TreeFlat: Building a (possible) faster tree for Rust, inspired by APL', matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://www.elmalabarista.com/blog/2022-flat-tree/', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'anonymous_they',
    points: null,
    story_text: null,
    comment_text: 'Affogata | Senior Backend Developer | Remote Tel Aviv +&#x2F;- 3 hours<p>Affogata (affogata.com) is a leading Customer Voice Insights platform. We process heavy loads of unstructured data to extract insights and enable seamless workflows in customer-obsessed organizations.<p>We&#x27;re looking for backend or full-stack Ruby on Rails developers.<p>Experience Musts: Web backend, dynamic languages, SQL.\nExperience Pluses: ElasticSearch, NodeJS, AWS, Terraform<p>Senior Ruby Engineer: <a href="https:&#x2F;&#x2F;rysolv.com&#x2F;jobs&#x2F;161ce69e-3b18-4108-b5ed-c9103fc46225" rel="nofollow">https:&#x2F;&#x2F;rysolv.com&#x2F;jobs&#x2F;161ce69e-3b18-4108-b5ed-c9103fc46225</a>',
    num_comments: null,
    story_id: 30515750,
    story_title: 'Ask HN: Who is hiring? (March 2022)',
    story_url: null,
    parent_id: 30515750,
    created_at_i: 1646261387,
    _tags: ['comment', 'author_anonymous_they', 'story_30515750'],
    objectID: 30534478,
    _highlightResult: {
      author: { value: 'anonymous_they', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "Affogata | Senior Backend Developer | Remote Tel Aviv +/- 3 hours<p>Affogata (affogata.com) is a leading Customer Voice Insights platform. We process heavy loads of unstructured data to extract insights and enable seamless workflows in customer-obsessed organizations.<p>We're looking for backend or full-stack Ruby on Rails developers.<p>Experience Musts: Web backend, dynamic languages, SQL.\nExperience Pluses: ElasticSearch, <em>NodeJS</em>, AWS, Terraform<p>Senior Ruby Engineer: <a href=\"https://rysolv.com/jobs/161ce69e-3b18-4108-b5ed-c9103fc46225\" rel=\"nofollow\">https://rysolv.com/jobs/161ce69e-3b18-4108-b5ed-c9103fc46225</a>", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'Ask HN: Who is hiring? (March 2022)', matchLevel: 'none', matchedWords: [] },
      story_url: { matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'vinay-sharma',
    points: null,
    story_text: null,
    comment_text: '<p><pre><code>  Remote: No\n  Contractual Work: No (Only Full Time)\n  Willing to relocate: Yes (US&#x2F;Canada Preferred)\n\n  Company: MakeMyTrip\n  Role: Software Engineer\n  Years of Experience: 3 Years\n  Location: Bengaluru, Karnataka\n\n  Technologies:\n    - React, Webpack, Babel, ESLint, Prettier\n    - React Native, Redux, MobX, React Query\n    - JavaScript, ES6, TypeScript, HTML5, CSS3\n    - Node, Express, GraphQL, RESTful, JWT\n    - SQL, MongoDB, Mongoose, Firebase, Azure\n    - GitHub, GitLab, Gerrit, BitBucket, JIRA\n</code></pre>\nRésumé&#x2F;CV: <a href="https:&#x2F;&#x2F;docs.google.com&#x2F;document&#x2F;d&#x2F;1PT7pksHJ7O0uJya30x4ul6bt05vX0gzeimlRVhrrPNo" rel="nofollow">https:&#x2F;&#x2F;docs.google.com&#x2F;document&#x2F;d&#x2F;1PT7pksHJ7O0uJya30x4ul6bt...</a><p>Email: vinaysharma7811@gmail.com<p>GitHub: <a href="https:&#x2F;&#x2F;github.com&#x2F;vinaysharma14&#x2F;" rel="nofollow">https:&#x2F;&#x2F;github.com&#x2F;vinaysharma14&#x2F;</a><p>LinkedIn: <a href="https:&#x2F;&#x2F;www.linkedin.com&#x2F;in&#x2F;vinaysharma-&#x2F;" rel="nofollow">https:&#x2F;&#x2F;www.linkedin.com&#x2F;in&#x2F;vinaysharma-&#x2F;</a><p>Stack Overflow: <a href="https:&#x2F;&#x2F;stackoverflow.com&#x2F;users&#x2F;11220479&#x2F;vinay-sharma" rel="nofollow">https:&#x2F;&#x2F;stackoverflow.com&#x2F;users&#x2F;11220479&#x2F;vinay-sharma</a><p>Rapid React: <a href="https:&#x2F;&#x2F;www.npmjs.com&#x2F;package&#x2F;rapid-react" rel="nofollow">https:&#x2F;&#x2F;www.npmjs.com&#x2F;package&#x2F;rapid-react</a><p>Hi, I am the Author of Rapid React and a Software Engineer at India&#x27;s Largest Travel Aggregator, MakeMyTrip. I have 3 years of experience developing FullStack Mobile and Web Apps with TypeScript, React, React Native, Node JS, Express, etc.<p>At present, I work in cross functional teams closely with product managers, designers, testers and and engineers in agile sprints with daily scrum and handle the development, debugging, deployment, documentation and post deployment monitoring of our React and React Native apps and a Express GraphQL Server Gateway.<p>I have open-sourced Rapid React, a CLI Tool that scaffolds tailored React apps, drastically reducing the development efforts. Rapid React is available on NPM and is ready to use without any additional setup. I am sure my experience with developing and publishing a library on NPM would be beneficial to your team.<p>Till date, I&#x27;ve spoken at 4 Tech Meetups, staying active in the React and React Native ecosystem. I also have experience mentoring 5 Engineers on how to grow in the field of Computer Science.<p>You would find a detailed description of my work experience, open-source contributions, tech talks, technical skills, education, and soft skills in my resume. I&#x27;m more than happy to connect and further discuss your requirements and what skills I’d bring to it.',
    num_comments: null,
    story_id: 30515748,
    story_title: 'Ask HN: Who wants to be hired? (March 2022)',
    story_url: null,
    parent_id: 30515748,
    created_at_i: 1646250406,
    _tags: ['comment', 'author_vinay-sharma', 'story_30515748'],
    objectID: 30531973,
    _highlightResult: {
      author: { value: 'vinay-sharma', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "<p><pre><code>  Remote: No\n  Contractual Work: No (Only Full Time)\n  Willing to relocate: Yes (US/Canada Preferred)\n\n  Company: MakeMyTrip\n  Role: Software Engineer\n  Years of Experience: 3 Years\n  Location: Bengaluru, Karnataka\n\n  Technologies:\n    - React, Webpack, Babel, ESLint, Prettier\n    - React Native, Redux, MobX, React Query\n    - JavaScript, ES6, TypeScript, HTML5, CSS3\n    - Node, Express, GraphQL, RESTful, JWT\n    - SQL, MongoDB, Mongoose, Firebase, Azure\n    - GitHub, GitLab, Gerrit, BitBucket, JIRA\n</code></pre>\nRésumé/CV: <a href=\"https://docs.google.com/document/d/1PT7pksHJ7O0uJya30x4ul6bt05vX0gzeimlRVhrrPNo\" rel=\"nofollow\">https://docs.google.com/document/d/1PT7pksHJ7O0uJya30x4ul6bt...</a><p>Email: vinaysharma7811@gmail.com<p>GitHub: <a href=\"https://github.com/vinaysharma14/\" rel=\"nofollow\">https://github.com/vinaysharma14/</a><p>LinkedIn: <a href=\"https://www.linkedin.com/in/vinaysharma-/\" rel=\"nofollow\">https://www.linkedin.com/in/vinaysharma-/</a><p>Stack Overflow: <a href=\"https://stackoverflow.com/users/11220479/vinay-sharma\" rel=\"nofollow\">https://stackoverflow.com/users/11220479/vinay-sharma</a><p>Rapid React: <a href=\"https://www.npmjs.com/package/rapid-react\" rel=\"nofollow\">https://www.npmjs.com/package/rapid-react</a><p>Hi, I am the Author of Rapid React and a Software Engineer at India's Largest Travel Aggregator, MakeMyTrip. I have 3 years of experience developing FullStack Mobile and Web Apps with TypeScript, React, React Native, <em>Node JS</em>, Express, etc.<p>At present, I work in cross functional teams closely with product managers, designers, testers and and engineers in agile sprints with daily scrum and handle the development, debugging, deployment, documentation and post deployment monitoring of our React and React Native apps and a Express GraphQL Server Gateway.<p>I have open-sourced Rapid React, a CLI Tool that scaffolds tailored React apps, drastically reducing the development efforts. Rapid React is available on NPM and is ready to use without any additional setup. I am sure my experience with developing and publishing a library on NPM would be beneficial to your team.<p>Till date, I've spoken at 4 Tech Meetups, staying active in the React and React Native ecosystem. I also have experience mentoring 5 Engineers on how to grow in the field of Computer Science.<p>You would find a detailed description of my work experience, open-source contributions, tech talks, technical skills, education, and soft skills in my resume. I'm more than happy to connect and further discuss your requirements and what skills I’d bring to it.", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'Ask HN: Who wants to be hired? (March 2022)', matchLevel: 'none', matchedWords: [] },
      story_url: { matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'remram',
    points: null,
    story_text: null,
    comment_text: 'The rest of the article explains why they won&#x27;t release <i>this product</i> opensource even though they love it. It is not disingenuous.<p>They released Mapperly under Apache-2.0, but I don&#x27;t know if you read that far.<p>If I tell you that &quot;I love NodeJS but this specific project needs to run on embedded so I used C&quot;, it doesn&#x27;t diminish my love of NodeJS or make it disingenuous. Love doesn&#x27;t mean you&#x27;ll always pick the thing <i>no matter what</i>, just a strong preference <i>over the other possible options</i>.',
    num_comments: null,
    story_id: 30527594,
    story_title: "Why Kreya isn't open source",
    story_url: 'https://kreya.app/blog/why-kreya-isnt-open-source/',
    parent_id: 30529522,
    created_at_i: 1646246467,
    _tags: ['comment', 'author_remram', 'story_30527594'],
    objectID: 30531043,
    _highlightResult: {
      author: { value: 'remram', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "The rest of the article explains why they won't release <i>this product</i> opensource even though they love it. It is not disingenuous.<p>They released Mapperly under Apache-2.0, but I don't know if you read that far.<p>If I tell you that &quot;I love <em>NodeJS</em> but this specific project needs to run on embedded so I used C&quot;, it doesn't diminish my love of <em>NodeJS</em> or make it disingenuous. Love doesn't mean you'll always pick the thing <i>no matter what</i>, just a strong preference <i>over the other possible options</i>.", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: "Why Kreya isn't open source", matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://kreya.app/blog/why-kreya-isnt-open-source/', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'dilipdasilva',
    points: null,
    story_text: null,
    comment_text: 'VDX.tv (<a href="http:&#x2F;&#x2F;www.vdx.tv" rel="nofollow">http:&#x2F;&#x2F;www.vdx.tv</a>) | Remote Engineers &amp; Developers | Full-time | REMOTE Established company (20+ years) looking for mature generalists to contribute remotely. You can be anywhere in the world so long as you:<p>● Are self-motivated and can work independently<p>● Have experience with many languages but can be effective in any language<p>● Appreciate that all mature software solutions accumulate technical debt and understand how to continually reduce debt and complexity<p>● Understand how to build highly reliable systems and be responsible for taking code to production<p>● Understand that code simplicity and readability are more important for long term maintainability<p>● Want to work on challenging problems and impactful work without being micromanaged<p>We&#x27;re hiring across disciplines for engineers who work with Unix and have fluent written and spoken English:<p>● Backend Servers: Strong in C, Concurrency and Distributed Computing<p>● Backend Servers: Strong in Java<p>● Data Science: Strong in Math&#x2F;Physics&#x2F;CS + Python, Linux, SQL<p>● Javascript: Strong in Javascript, HTML5 canvas, CSS, JS Video Libraries, Developed frameworks<p>● Frontend Development: Strong in Angular &#x2F; React &#x2F; Vue JS and Node JS, Data intensive dashboards, frontend design and architecture patterns<p>● System Infrastructure: Strong in private and public cloud, infrastructure as code, scripting and programming.<p>Full-time only. Remote only. We start out on a full-time trial contract basis for up to 3 months and use this period as an extended work interview for both sides to assess fit for long-term employment.<p>Please email hn.remote.jobs@vdx.tv if this sounds like the right fit for you.',
    num_comments: null,
    story_id: 30515750,
    story_title: 'Ask HN: Who is hiring? (March 2022)',
    story_url: null,
    parent_id: 30515750,
    created_at_i: 1646249120,
    _tags: ['comment', 'author_dilipdasilva', 'story_30515750'],
    objectID: 30531659,
    _highlightResult: {
      author: { value: 'dilipdasilva', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "VDX.tv (<a href=\"http://www.vdx.tv\" rel=\"nofollow\">http://www.vdx.tv</a>) | Remote Engineers &amp; Developers | Full-time | REMOTE Established company (20+ years) looking for mature generalists to contribute remotely. You can be anywhere in the world so long as you:<p>● Are self-motivated and can work independently<p>● Have experience with many languages but can be effective in any language<p>● Appreciate that all mature software solutions accumulate technical debt and understand how to continually reduce debt and complexity<p>● Understand how to build highly reliable systems and be responsible for taking code to production<p>● Understand that code simplicity and readability are more important for long term maintainability<p>● Want to work on challenging problems and impactful work without being micromanaged<p>We're hiring across disciplines for engineers who work with Unix and have fluent written and spoken English:<p>● Backend Servers: Strong in C, Concurrency and Distributed Computing<p>● Backend Servers: Strong in Java<p>● Data Science: Strong in Math/Physics/CS + Python, Linux, SQL<p>● Javascript: Strong in Javascript, HTML5 canvas, CSS, JS Video Libraries, Developed frameworks<p>● Frontend Development: Strong in Angular / React / Vue JS and <em>Node JS</em>, Data intensive dashboards, frontend design and architecture patterns<p>● System Infrastructure: Strong in private and public cloud, infrastructure as code, scripting and programming.<p>Full-time only. Remote only. We start out on a full-time trial contract basis for up to 3 months and use this period as an extended work interview for both sides to assess fit for long-term employment.<p>Please email hn.remote.jobs@vdx.tv if this sounds like the right fit for you.", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'Ask HN: Who is hiring? (March 2022)', matchLevel: 'none', matchedWords: [] },
      story_url: { matchedWords: [] },
    },

  },
  {
    title: null,
    url: null,
    author: 'mike_hearn',
    points: null,
    story_text: null,
    comment_text: 'XML is actually pretty good at declaring UI. It&#x27;s the sort of thing the language was designed for - a UI is a tree of attributed nodes, and that&#x27;s also what XML gives you.',
    num_comments: null,
    story_id: 30530889,
    story_title: 'The decline and fall of Java on the desktop',
    story_url: 'https://jdeploy.substack.com/p/the-decline-and-fall-of-java-on-the',
    parent_id: 30531799,
    created_at_i: 1646254473,
    _tags: ['comment', 'author_mike_hearn', 'story_30530889'],
    objectID: 30532988,
    _highlightResult: {
      author: { value: 'mike_hearn', matchLevel: 'none', matchedWords: [] },
      comment_text: {
        value: "XML is actually pretty good at declaring UI. It's the sort of thing the language was designed for - a UI is a tree of attributed <em>nodes</em>, and that's also what XML gives you.", matchLevel: 'full', fullyHighlighted: false, matchedWords: ['nodejs'],
      },
      story_title: { value: 'The decline and fall of Java on the desktop', matchLevel: 'none', matchedWords: [] },
      story_url: { value: 'https://jdeploy.substack.com/p/the-decline-and-fall-of-java-on-the', matchLevel: 'none', matchedWords: [] },
    },

  },
  {
    title: 'Password Manager for Home Network (free and opensrc)',
    url: 'https://github.com/SvenHerr/Pw-manager-nodejs',
    author: 'svenherrmann',
    points: 4,
    story_text: null,
    comment_text: null,
    num_comments: 2,
    story_id: null,
    story_title: null,
    story_url: null,
    parent_id: null,
    created_at_i: 1646243770,
    _tags: ['story', 'author_svenherrmann', 'story_30530377'],
    objectID: 30530377,
    _highlightResult: {
      author: { value: 'svenherrmann', matchLevel: 'none', matchedWords: [] }, comment_text: { matchedWords: [] }, story_title: { matchedWords: [] }, story_url: { matchedWords: [] },
    },

  },
];

beforeEach(async () => {
  await Article.deleteMany({});

  await Article.create(initialArticles[0]);
  await Article.create(initialArticles[1]);
  await Article.create(initialArticles[2]);
  await Article.create(initialArticles[3]);
  await Article.create(initialArticles[4]);
  await Article.create(initialArticles[5]);
  await Article.create(initialArticles[6]);
  await Article.create(initialArticles[7]);
  await Article.create(initialArticles[8]);
  await Article.create(initialArticles[9]);
  await Article.create(initialArticles[10]);
});

test('articles are returned as json', async () => {
  await api
    .get('/api/articles')
    .expect(200)
    .expect('Content-Type', /application\/json/);
});

test('the first article has author ahungry...', async () => {
  const response = await api.get('/api/articles');
  const authors = response.body.docs.map((article) => article.author);
  expect(authors).toContain('ahungry');
});

test('there are five articles', async () => {
  const response = await api.get('/api/articles');
  expect(response.body.docs).toHaveLength(5);
});

test('pagination works', async () => {
  const pageIndex = 2;
  const response = await api.get(`/api/articles?pageIndex=${pageIndex}`);
  const authors = response.body.docs.map((article) => article.author);
  const pageArticles = initialArticles.slice((pageIndex - 1) * 5, pageIndex * 5);
  pageArticles.forEach((article, index) => expect(article.author).toContain(authors[index]));
});

test('articles filter by author', async () => {
  const authorQuery = 'anonymous_they';
  const response = await api.get(`/api/articles?author=${authorQuery}`);
  const articlesFiltered = initialArticles.filter((article) => article.author === authorQuery);
  const articlesResponse = response.body.docs;
  expect(articlesResponse[0].author).toContain(authorQuery);
  expect(articlesResponse).toHaveLength(articlesFiltered.length);
});

test('articles filter by _tags', async () => {
  const tags = ['comment', 'test'];
  let url = '/api/articles?';
  tags.forEach((tag) => {
    url += `_tags[]=${tag}&`;
  });

  const response = await api.get(url);
  const articlesFiltered = initialArticles.filter(
    (article) => article._tags.every((tag) => tags.includes(tag)),
  );
  const articlesSliced = articlesFiltered.slice(0, 5);
  const articlesResponse = response.body.docs;
  expect(articlesResponse).toHaveLength(articlesSliced.length);
});

test('count the length of articles after a post', async () => {
  await api.post('/api/articles').expect(201).expect('Content-Type', /application\/json/);

  const response = await api.get('/api/articles');
  const articlesResponseLength = response.body.totalDocs;
  expect(articlesResponseLength).toBe(initialArticles.length + 20);
});

test('the article has been deleted by id', async () => {
  const response = await api.get('/api/articles');
  const articleToBeDeleted = response.body.docs[0];

  await api.delete(`/api/articles/${articleToBeDeleted._id}`).expect(200).expect('Content-Type', /application\/json/);

  const newResponse = await api.get('/api/articles');
  const articles = newResponse.body.totalDocs;
  expect(articles).toBe(initialArticles.length - 1);
});

afterAll(() => {
  mongoose.connection.close();
  server.close();
});
