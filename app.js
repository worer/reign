const express = require('express');
const schedule = require('node-schedule');
const fetch = require('node-fetch');

const expressConfig = require('./config/express');
const connectDB = require('./config/database');
const routes = require('./routes');
const swaggerDocs = require('./config/swagger');
const { createArticles } = require('./api/article/article.service');

const app = express();

expressConfig(app);

const PORT = process.env.PORT || 8080;

if (process.env.NODE_ENV !== 'test') {
  schedule.scheduleJob('0 */1 * * *', async () => {
    const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
    const response = await fetch(url);
    const data = await response.json();
    const { hits: newArticles } = data;
    await createArticles(newArticles);
    console.log('it works');
  });
}

const server = app.listen(PORT, () => {
  // connect to database
  connectDB();

  // Routes
  routes(app);

  // Swagger
  swaggerDocs(app, 8080);

  console.log('server listening on port', PORT);
});

module.exports = { app, server };
