/* eslint-disable no-underscore-dangle */
const fetch = require('node-fetch');
const { getAllArticles, createArticles, deleteArticle } = require('./article.service');

async function getAllArticlesHandler(req, res) {
  try {
    const pageIndex = parseInt(req.query.pageIndex, 10) || 1;
    const { author, _tags, title } = req.query;
    const query = { author, _tags, title };
    if (!author) delete query.author;
    if (!_tags) delete query._tags;
    if (!title) delete query.title;
    const articles = await getAllArticles(query, pageIndex);
    return res.status(200).json(articles);
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
}

async function createArticlesHandler(req, res) {
  const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
  const response = await fetch(url);
  const data = await response.json();
  const { hits: newArticles } = data;
  const articles = await createArticles(newArticles);
  return res.status(201).json(articles);
}

async function deleteAticleHandler(req, res) {
  try {
    const { id } = req.params;
    const article = await deleteArticle(id);
    if (!article) {
      return res.status(404).json({
        message: `Article not found with id: ${id}`,
      });
    }
    return res.status(200).json(article);
  } catch (error) {
    return res.status(500).json({
      error: error.message,
    });
  }
}

module.exports = {
  createArticlesHandler,
  getAllArticlesHandler,
  deleteAticleHandler,
};
