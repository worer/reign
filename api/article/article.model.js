const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const ArticleSchema = mongoose.Schema(
  {
    title: {
      type: String,
    },
    url: {
      type: String,
    },
    author: {
      type: String,
    },
    points: {
      type: Number,
    },
    story_text: {
      type: String,
    },
    comment_text: {
      type: String,
    },
    num_comments: {
      type: Number,
    },
    story_id: {
      type: Number,
    },
    story_title: {
      type: String,
    },
    story_url: {
      type: String,
    },
    parent_id: {
      type: Number,
    },
    created_at_i: {
      type: Number,
    },
    _tags: {
      type: Array,
    },
    objectID: {
      type: Number,
    },
    _highlightResult: {
      author: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: Array,
        },
      },
      comment_text: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        fullyHighlighted: {
          type: Boolean,
        },
        matchedWords: {
          type: Array,
        },
      },
      story_title: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: Array,
        },
      },
      story_url: {
        value: {
          type: String,
        },
        matchLevel: {
          type: String,
        },
        matchedWords: {
          type: Array,
        },
      },
    },
  },
);

ArticleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Article', ArticleSchema);
