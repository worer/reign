/* eslint-disable no-underscore-dangle */
const Article = require('./article.model');

/**
 * Get all articles
 * @returns all articles
 */
async function getAllArticles(query, pageIndex) {
  if (query._tags) {
    const newQuery = { ...query };
    delete newQuery._tags;
    const articlesFilteredByTags = await Article.paginate(
      { _tags: { $all: [...query._tags] }, ...newQuery },
      { page: pageIndex, limit: 5 },
    );
    return articlesFilteredByTags;
  }
  const articles = await Article.paginate(query, { page: pageIndex, limit: 5 });
  return articles;
}

/**
 * Create a new article
 * @param {Object} article Article to create
 * @returns Article created
 */
async function createArticles(articles) {
  const newArticles = await Article.create(articles);
  return newArticles;
}

/**
 * Delete an article
 * @param {String} id Identifier of the article to be deleted
 * @returns article deleted
 */
async function deleteArticle(id) {
  const deletedArticle = await Article.findByIdAndDelete(id);
  return deletedArticle;
}

module.exports = {
  getAllArticles,
  createArticles,
  deleteArticle,
};
