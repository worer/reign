const { Router } = require('express');

const { createArticlesHandler, getAllArticlesHandler, deleteAticleHandler } = require('./article.controller');

const router = Router();

/**
 * @swagger
 * components:
 *  schemas:
 *    Article:
 *      type: object
 *      properties:
 *        _id:
 *          type: string
 *          description: the id of the article
 *        title:
 *          type: string
 *          description: the title of the article
 *        url:
 *          type: string
 *          description: the url of the article
 *        author:
 *          type: string
 *          description: the author of the article
 *        points:
 *          type: integer
 *          description: the points that the article has recieve
 *        story_text:
 *          type: string
 *          description: the story text of the article
 *        comment_text:
 *          type: string
 *          description: the comment text of the article
 *        num_comments:
 *          type: integer
 *          description: the num of comments that the article has
 *        story_id:
 *          type: integer
 *          description: the story id of the article
 *        story_title:
 *          type: string
 *          description: the story title of the article
 *        story_url:
 *          type: string
 *          description: the story url of the article
 *        parent_id:
 *          type: integer
 *          description: the parent id of the article
 *        created_at_i:
 *          type: integer
 *          description: the date of the article has been created
 *        _tags:
 *          type: array
 *          items:
 *            type: string
 *        objectID:
 *          type: integer
 *          description: the objectID of the article
 *        __highlightResult:
 *          type: object
 *          properties:
 *            author:
 *              type: object
 *              properties:
 *                value:
 *                  type: string
 *                matchLevel:
 *                  type: string
 *                matchedWords:
 *                  type: array
 *                  items:
 *                    type: string
 *            comment_text:
 *              type: object
 *              properties:
 *                value:
 *                  type: string
 *                matchLevel:
 *                  type: string
 *                fullyHighlighted:
 *                  type: boolean
 *                matchedWords:
 *                  type: array
 *                  items:
 *                    type: string
 *            story_title:
 *              type: object
 *              properties:
 *                value:
 *                  type: string
 *                matchLevel:
 *                  type: string
 *                matchedWords:
 *                  type: array
 *                  items:
 *                    type: string
 *            story_url:
 *              type: object
 *              properties:
 *                value:
 *                  type: string
 *                matchLevel:
 *                  type: string
 *                matchedWords:
 *                  type: array
 *                  items:
 *                    type: string
 *      example:
 *        title: null
 *        url: null
 *        author: ahungry
 *        points: null
 *        story_text: null
 *        comment_text: It&#x27;d be nice if someone on the Snowflake...
 *        num_comments: null
 *        story_id: 30533799
 *        story_title: Snowflake acquires Streamlit...
 *        story_url: https://techcrunch.com/2022/03/02/snowflake-acquires-streamlit-for-800m-to-help-customers-build-data-based-apps/
 *        parent_id: 30533799
 *        created_at_i: 1646277903
 *        _tags: [comment, author_ahungry]
 *        objectID: 30536770
 *        _highlightResult: {}
 */

/**
 *  @swagger
 *  /api/articles:
 *    get:
 *      summary: return all the articles from the bd
 *      tags: [Article]
 *      responses:
 *        200:
 *          description: all articles
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  docs:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Article'
 *                  totalDocs:
 *                    type: integer
 *                  limit:
 *                    type: integer
 *                  totalPages:
 *                    type: integer
 *                  page:
 *                    type: integer
 *                  paginCounter:
 *                    type: integer
 *                  hasPrevPage:
 *                    type: boolean
 *                  hasNextPage:
 *                    type: boolean
 *                  prevPage:
 *                    type: integer
 *                  nextPage:
 *                    type: integer
 */
router.get('/', getAllArticlesHandler);

/**
 *  @swagger
 *  /api/articles:
 *    post:
 *      summary: Fetch articles from an url and post it into the DB
 *      tags: [Article]
 *      responses:
 *        201:
 *          description: new articles has been created in the DB
 */
router.post('/', createArticlesHandler);

/**
 *  @swagger
 *  /api/articles/{id}:
 *    delete:
 *      summary: returns the article deleted
 *      tags: [Article]
 *      parameters:
 *        - in: path
 *          name: id
 *          schema:
 *            type: string
 *          required: true
 *          description: the article id
 *      responses:
 *        200:
 *          description: the article has been deleted
 */
router.delete('/:id', deleteAticleHandler);

module.exports = router;
