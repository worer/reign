# Reign Challenge for Junior Back End Developer !

### This is a small API challenge to test my knowledge of **Back End Development** and _related technologies_

</br>

![Logo](https://d33wubrfki0l68.cloudfront.net/cc301e98a199a8b59325d28a96b7d61643efe70f/04637/static/8cec8c20524baf887c3866345b536aa7/logo-reign-full.svg)

</br>
</br>

# 🔥 Installation

Create a folder where you want to clone the application.

Open the terminal in that folder type.

`git clone https://gitlab.com/worer/reign.git`

`cd reign`

# ▶️ Usage

You need to install the dependencies.

`npm install`

Then you need to create a _.env_ file at the root of the project and paste this block of code

```
DB_URI = mongodb://localhost:27020/mydb
DB_URI_TEST = mongodb://localhost:27020/mydb-test
PORT =  8080
```

## 🌾 Seed Database

You need to seed the database.

Clear all the data in the database.

`npm run articles:destroy`

Import the seed into the database.

`npm run articles:import`

## 🏃 Run the app

To run de app as a developer you can type the next command.

`npm run dev`

# 🟠 REST API

The REST API for this challenge is described below.

| Method | Endpoint          | Query Params                      | Description                                                                                                                                                 |
| ------ | ----------------- | --------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| GET    | /api/articles     | pageIndex, author, title, \_tag[] | Gets all the articles stored in the database, pageIndex should be a number, author should be a string, title should be a string, \_tag[] should be a string |
| POST   | /api/articles     |                                   | Fetch the articles from the url: https://hn.algolia.com/api/v1/search_by_date?query=nodejs and save it into the database                                    |
| DELETE | /api/articles/:id |                                   | Delete an article by id, the id should be an object id as a parameter.                                                                                      |

### Example

</br>

> GET /api/articles

![Get](https://media.giphy.com/media/nHJBIiyN9vqiCimgXu/giphy.gif)

> POST /api/articles

![Post](https://media.giphy.com/media/2z7ywf8NeGUWF74kiB/giphy.gif)

> DELETE /api/articles

![Delete](https://media.giphy.com/media/pu44NHfCCR10bjMuaz/giphy.gif)

# 📗 Swagger

Once the server is running

`npm run dev`

You can see the swagger documentation by visiting this link.

🔗 http://localhost:8080/docs/

You will see this.

![Swagger](https://media.giphy.com/media/QlJoYO91JEaaWTa5NG/giphy.gif)

# 🧪 Test

You can run the test.

`npm run test`

Also, you can see the coverage.

`npm run test:coverage`

![Coverage](https://media.giphy.com/media/jTlh00KX4oZFzhhBMf/giphy.gif)

# Postman

You can import _Reign_Postman.json_ file to your postman to try the endpoints.

# 👨‍💻 Author

### **Walther Vergaray**

[LinkedIn](https://www.linkedin.com/in/vergaray-moreno-walther/)

</br>

# 🍁 License

**ISC**
